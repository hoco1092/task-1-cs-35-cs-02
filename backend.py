import sys
from PyQt5.uic import loadUi
from PyQt5.QtWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets
import csv
import pandas as pd
class Mainwindow(QMainWindow()):
    def __init__(self):
        super(Mainwindow,self).__init__()
        loadUi('estamp.ui',self) # Here we imported the QT Designer GUI File.
        # Command to remove the default Windows Frame Design.
        self.setWindowFlag(QtCore.Qt.FramelessWindowHint)
        # Command to make the backgroud of Window transparent.
        self.setAttribute(QtCore.Qt.WA_TranslucentBackground)
        #These 2 lines are used to put funnctions on close and minimize buttons.
        self.MinimizeButton.clicked.connect(lambda: self.showMinimized())
        self.CrossButton.clicked.connect(lambda: self.close())
        #Function to Load Data in combo Box
        self.LoadDatainComboBox()
        # Function to load the previous data of student at the start of program.
        self.loadData()
        # Estamp Form Button Events
        self.btnSaveFrm.clicked.connect(self.saveForm)
        self.btnResetFrm.clicked.connect(self.restForm)
        self.btnIndexPage.clicked.connect(self.openIndexPage)
        ######################################################
    # This is a helping Function to load the content of the table after every event.
    def saveFrom(self):
        #All labels and dropdowns save
        district=self.comboDistrict.currentText()
        tehsil=self.combotehsil.currentText()
        stampType=self.combostampType.currentText()
        deedName=self.comboDeedName.currentText()
        aName=self.txtAName.text()
        aCNIC=self.txtACNIC.text()
        aEmail=self.txtAContact.text()
    def resetForm(self):
        #All labels and dropdowns to empty
        self.comboBox.clear()       # delete all items from comboBox
        self.comboBox.addItems(self.comboData) # add the actual content of self.comboData
    def loadData(self):
        with open('estamp.csv', "r",encoding="utf-8") as eStampForms:
            roww = 0
            data = list(csv.reader(eStampForms))
            D2Table(self,data,self.eStampsTable)
    def D2Table(self, array, qtable):
        qtable.setColumnCount(len(array[0])) #rows and columns of table
        qtable.setRowCount(len(array))
        for row in range(len(array)): # add items from array to QTableWidget
            for column in range(len(array[0])):
                item = array[row][column]
                qtable.setItem(row, column, QTableWidgetItem(item))
    def LoadDatainComboBox(self):
        district="districts.csv"
        stamps="Stamps.csv"
        dicDistrict=file2Dic(district)
        dicStamps=file2Dic(stamps)
    def file2Dic(file):
        two_D=pd.read_csv(file)
        Dic={}
        for i in two_D:    
            Dic[i[0]] =i[1:]
            
class StampPaper():
    def __init__(self,deedName,kind,article):
        self.deedName=deedName
        self.kind=kind
        self.article=article
    